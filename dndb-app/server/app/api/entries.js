'use strict';

var mongoose = require('mongoose'),
    Entry = mongoose.model('Entry'),
    queryBuilder = require(_root + '/lib/queries/from-request');

module.exports = function entryRoutes(router) {
    ['entries','entry'].forEach(page => {

        router.use('/' + page + '/:entryId', module.exports.get);
        router.use('/' + page, module.exports.getAll);

    });
};

module.exports.get =
function(req, res) {
    res.json(req.entry);
};

module.exports.getAll =
function(req, res, next) {
    var query = Entry.find();
    queryBuilder.many(query, req, {
        });

    query
        .execAsync()
        .then(results => res.json(results))
        .catch(next);
};
