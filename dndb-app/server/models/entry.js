'use strict';

var slug = require('slug');

module.exports = function(mongoose) {
    mongoose = mongoose || require('mongoose');
    var Schema = mongoose.Schema;

    var EntrySchema = new Schema({
        name:       { type:String, index:true },
        category:   { type:String, index:true },
        body_str:   String,
        body_html:  String,

        type: String,

        attributes: {
            type:           String,
            sourcebook:     String,
            campaign:       String,
            skills:         String,
            powerSource:    String,
            keyAbilities:   String,
            roleName:       String,
            groupRole:      String,
            combatRole:     String,
            level:          Number,
            alignment:      String,
            prereqs:        String,
            tier:           String,
            cost:           String,
            rarity:         String,
            itemCategory:   String,
            actionType:     String,
            className:      String,
            size:           String,
            raceAttributes: String,
            componentCost:  String,
            price:          String,
            keySkills:      String,
        }
    }, {
       toObject:    { virtuals:true },
       toJSON:      { virtuals:true },
    });

    EntrySchema.virtual('category_slug')
        .get(function() {
            return slug(this.category.toLowerCase());
        });

    EntrySchema.methods = {
    };

    return mongoose.model('Entry', EntrySchema);
};
