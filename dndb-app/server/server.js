'use strict';

let debug = require('debug')('dndb:server'),
    chalk = require('chalk');

// Make sure we're using the right directory
process.chdir(__dirname);

require('./root');

// Module Dependencies
let express = require('express'),
    async = require('async');

// Load configurations
let env = process.env.NODE_ENV = process.env.NODE_ENV || 'dev',
    config = require('./config/config'),
    sockets = require('./config/sockets');

let app = express();

// Configuration
require('./config/express')(app);

// Show yourself
exports = module.exports = (window, cb) => {
    let server,
        port = config.port;

    // Start the application
    async.series([
        (cb) => {
            server = app.listen(port, () => {
                debug('Express application started on port `' + chalk.yellow('%s') + '`', port);
                debug('Environment: ' + chalk.yellow('%s'), env);
                cb();
            });
        },
        (cb) => {
            sockets.start(server, window, cb);
        },
    ], () => {
        setTimeout(() => {
            cb(app);
        }, 100);
    });
};
