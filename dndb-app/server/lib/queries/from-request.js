/**
 * Build a query based on request parameters
 *
 * @module lib/query
 */

'use strict';

var _debug = require('debug')('rest-api:lib:build-query'),
    chalk = require('chalk');

var mongoose = require('mongoose');
// var models = require('../models');

/**
 * Build a query for handling many objects
 *
 * @param {Object} query - starting mongoose query to be built
 * @param {Object} req - request object to determine how to query
 * @param {Object} options
 *
 * @returns {Object} *
 */
exports.many =
function buildManyQuery(query, req, options) {
    options = options || {};
    var debug = options.debug || _debug;

    var search =    req.query.search || req.query.q,
        terms =     req.query.searchTerms || req.query.terms,
        page =      parseInt(req.query.page) || 1,
        limit =     parseInt(req.query.limit),
        count =     req.query.count,
        sort =      req.query.sort,
        exclude =   req.query.exclude || req.query.excludes || req.query.not;

    if (search) {
        // Trim whitespace
        if (search.trim) search = search.trim();

        // var regex = new RegExp(search, 'i');
        var regex = new RegExp('(\\b' + search + ')', 'i');

        if (terms) terms = terms.split(',');
        if (!terms || !terms.length) {
            terms = [
                'name',
                'category',
                'body_str',
            ];
        }

        debug('searching terms: ', terms);

        query.or(terms.map(function(term) {
            var result = {};
            result[term] = regex;
            return result;
        }));
    }

    // Only do these operations when we're not dealing with a count request,
    // count requests only return a number, so all of this would be worthless
    // otherwise
    if (!count) {
        // Pagination
        if (limit !== 0 || !isNaN(limit)) {
            if (limit < 0 || isNaN(limit)) limit = 20;

            debug('query.limit -> ' + chalk.blue('%s'), limit);
            debug('query.page  -> ' + chalk.blue('%s'), page);
            query.limit(limit)
                         .skip((page - 1) * limit);
            debug(chalk.yellow('--- q.1 ---'));
        }

        // Sort results
        if (sort) {
            // field,-asc => field -asc
            sort = sort.replace(/,/g, ' ');
            query.sort(sort);
        }
    }

    /**
     * Compute the 'start' date for the provided age
     * @param intAge {int} The age for which to compute the start date
     * @returns {Date}
     */
    function getDateForAge(intAge) {
        var start = new Date();
        start.setFullYear(start.getFullYear() - intAge);
        return start;
    }

    /*
     * Make all fields query-able
     */
    function searchFields(fieldName, query) {
        var keys = fieldName;

        if (!Array.isArray(keys))
            keys = [keys];

        keys.forEach(function(key) {
            var modelField, paramName, isAgeAttribute = false;

            if (typeof(key) === 'object') {
                modelField = key.field || key.model ||
                             key.fieldName || key.modelName ||
                             key.modelField;
                paramName = key.param || key.queryParam || key.paramName;
                isAgeAttribute = key.isAgeAttribute;
            } else {
                modelField =
                paramName = key;
            }

            if (!req.query[paramName]) return;

            var q = {};
                q[modelField] = {};
            var isSearchingFor = true;
            var whereIs = [],
                whereNot = [];

            req.query[paramName]
               .split(',')
               .forEach(function(value) { // Trim white-space
                   if (value.trim)
                       value = value.trim();

                   var is = true;
                   if (value[0] === '-') {
                       value = value.substr(1);
                       is = false;
                   }

                   // Range-related logic
                   if (/^(\$([gl]te?|eq):)/.test(value)) {
                       var searchOp = value.split(':')[0];
                       var isIncluding = false;
                       // Placeholder for now, useful if we have parallel
                       // functionality in the request endpoints
                       switch(searchOp) {
                           case '$gte':
                               searchOp = isAgeAttribute? '$lte': '$gte';
                               isIncluding = true;
                               break;
                           case '$lte':
                               searchOp = isAgeAttribute? '$gte': '$lte';
                               isIncluding = true;
                               break;
                           case '$gt':
                               searchOp = isAgeAttribute? '$lt': '$gt';
                               break;
                           case '$lt':
                               searchOp = isAgeAttribute? '$gt': '$lt';
                               break;
                       }

                       var opValue = value.substr(value.indexOf(':') + 1);
                       var isNumber = !isNaN(opValue);
                       // var canBeDate = models.isTypeInSchema(query.model, 'Date', modelField);

                       if (isAgeAttribute && isNumber) {
                           let age = parseInt(opValue, 10) + (isIncluding? 0: 1);
                           q[modelField][searchOp] = getDateForAge(age);
                           return;
                       }

                       q[modelField].$exists = true;
                       // If the value isn't a number, assume that it's a date
                       if (isNumber)
                            q[modelField][searchOp] = opValue;
                       // else if (canBeDate)
                       //      q[modelField][searchOp] = new Date(opValue);
                       else
                            q[modelField][searchOp] = opValue;

                       return;
                   } ///^(\$[gl]te?:)/.test(value)


                   // allow to search by RegExp only for fields which are defined with data type "String" in schema definition (see #DYNQRA-161)
                   // if (req.query.regexFind && models.isStringInSchema(query.model, modelField))
                   //     value = new RegExp(value, 'i');

                   if (isAgeAttribute && !isNaN(value)) {
                       let age = parseInt(value, 10);
                       // add the exact year criteria (allow to search by exact year (see profile.getAll))
                       q[modelField] = {'$exists': true, '$gt': getDateForAge(age + 1), '$lt': getDateForAge(age + 0)};
                       return;
                   }

                   if (is) {
                       whereIs.push(value);
                   } else {
                       whereNot.push(value);
                   }
               });

            if (modelField[0] === '-') {
                isSearchingFor = false;
                modelField = modelField.substr(1);
            }

            if (isSearchingFor) {
                if (whereIs.length)  q[modelField].$in = whereIs;
                if (whereNot.length) q[modelField].$nin = whereNot;
            } else {
                if (whereIs.length)  q[modelField].$nin = whereIs;
                if (whereNot.length) q[modelField].$in = whereNot;
            }

            debug('query: ', q);
            query.where(q);
        });
    }

    var ignoredKeys = [
        '_id', 'type', 'loc'
    ];

    debug(chalk.yellow('--- 1 ---'));

    for (var key in query.schema.paths)
        if (ignoredKeys.indexOf(key) === -1) {
            searchFields([key, '-' + key], query);
        }

    debug(chalk.yellow('--- 2 ---'));

    // Search by optional terms
    if (options.queryTerms && Array.isArray(options.queryTerms)) {
        options.queryTerms.forEach(function(row) {
            searchFields(row, query);
        });
    }

    // Exclude certain ids from the result set
    if (exclude) {
        // Select where the id is not in the list of excluded ids
        var excludes = exclude.split(',')
                              .map(function(value) { // Trim white-space
                                  if (value.trim)
                                      return value.trim();
                                  else
                                      return value;
                              });

        query.where({ _id: { $nin:excludes } });
    }

    if (count) {
        query.count();
    }

    exports.common(query, req, options);

    return query;
};

/**
 * Build a query for handling single objects
 *
 * @param {Object} query - starting mongoose query to be built
 * @param {Object} req - request object to determine how to query
 * @param {Object} options
 *
 * @returns {Object} *
 */
exports.one =
function buildOneQuery(query, req, options) {
    options = options || {};
    options.ignoreCountQuery = true;
    // var debug = options.debug || _debug;

    exports.common(query, req, options);

    return query;
};

/**
 * Common query options
 *
 * @param {Object} query - starting mongoose query to be built
 * @param {Object} req - request object to determine how to query
 * @param {Object} options
 *
 * @returns {Object} *
 */
exports.common =
function buildQueryCommon(query, req, options) {
    options = options || {};
    // var debug = options.debug || _debug;

    var include =   req.query.include,
        select =    req.query.select;

    if (options.ignoreCountQuery || !req.query.count) {
        // Populate selected fields
        if (include) {
            var includes = include.split(',');

            includes.forEach(function(include) {
                if (options.includeModel &&
                    options.includeModel[include] &&
                    mongoose.models[ options.includeModel[include] ])
                    query.populate(include, null, options.includeModel[include]);
                else
                    query.populate(include);
            });
        }

        if (select) {
            query.select(select.replace(/,/g, ' '));
        }
    }


    return query;
};
