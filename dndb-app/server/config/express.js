'use strict';

/**
 * Express Setup
 *
 * Setup and configure the express application
 */

let debug = require('debug')('dndb:config:express'),
    chalk = require('chalk');

let bodyParser = require('body-parser');

require(_root + '/config/promises');

module.exports = function(app) {

    app.set('json spaces', 4);
    app.set('showStackError', true);
    app.enable('jsonp callback');

    // No logger on test environment
    if (process.env.NODE_ENV !== 'test') {
        app.use(require('morgan')('dev'));

        process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
        debug(chalk.yellow('WARNING') + ': allowing self-signed certs for ' +
            'development. Make sure to user proper ssl certifications in ' +
            'production environment');
    }

    app.use(require('method-override')());
    app.use(require('compression')());

    // Setup Db
    let db = require(_root + '/config/database')(app);

    // Setup view engine
    require(_root + '/config/views')(app);

    // parse application/json requests
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // Continue to routing,
    require(_root + '/config/routing')(app, db);

    // Error handling
    // require(_root + '/config/errors')(app);
};
