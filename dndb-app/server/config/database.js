'use strict';

/**
 * Database Setup
 *
 * Configure and setup mongo and its components
 */
var debug = require('debug')('dndb:config:database');

var config = require('./'),
    mongoose = require('mongoose');

var cred = cred && cred.MONGOLAB && cred.MONGOLAB.MONGOLAB_URI;
debug('CC MONGO CREDENTIALS: ', cred);

module.exports = function() {

    // Connect mongoose to the database
    mongoose.connect(cred || config.db);

    // Load models
    // require('dndb-schemas')(mongoose);
    require(_root + '/models/entry')(mongoose);

    return mongoose;
};
