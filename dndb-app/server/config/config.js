var _ = require('underscore');

// Application Config
module.exports = _.extend(
    require(_root + '/config/env/all.js'),
    require(_root + '/../env/' + process.env.NODE_ENV + '.json') || {}
);
