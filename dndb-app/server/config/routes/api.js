'use strict';
/**
 * API
 * Routes that manage API methods
 */

let debug = require('debug')('dndb:routes:api'),
    chalk = require('chalk');

let config = require(_root + '/config'),

    // CheckIf = require(_root + '/lib/check-if'),
    // is = require(_root + '/lib/is'),
    // checkIf = require(_root + '/config/auth'),

    // appApi = require(_root + '/app/api'),
    info = require(_root + '/../package'),
    express = require('express'),
    router = express.Router();

module.exports = function(app, db) {

    // Make sure that requests are marked as being part of the api
    router.use(function(req, res, next) {
        req.isApi = true;
        next();
    });


    // /**
    //  * Add the user and other information to the request
    //  */
    // router.use(require(_root + '/config/middlewares/auth-info'));

    require('../params')(router, db);

    router.use('/info', function(req, res) {
        res.json({
            'started': config.started,
            'uptime': Date.now() - config.started.getTime(),
            'app.name': config.app.name,
            'port': config.port,
            'version': info.version,
        });
    });


    router.use('/now', function(req, res) {
        let moment = require('moment')();
        let now = new Date();

        res.json({
            timezone: process.env.TZ || '---',
            date: {
                now:now,
                unix:now.valueOf(),
                str:now.toString(),
                iso:now.toISOString(),
            },
            moment: {
                unix:moment.valueOf(),
                str:moment.format('LLL'),
                iso:moment.format(),
            }

        });
    });

    require(_root + '/app/api/entries')(router);

    app.use('/api', router);
};
