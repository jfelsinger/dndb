'use strict';
/**
 * Static
 * Routes that manage static methods
 */

let express = require('express'),
    compression = require('compression'),
    config = require('../config');

module.exports = function(app) {
    let staticLocation = config.staticLocation || 'client';
    let assetsLocation = config.assetsLocation || 'client';

    app.use(compression());

    // Static client scripts
    app.use('/assets', express.static(config.root + '/../' + assetsLocation));
    app.use(express.static(config.root + '/../' + staticLocation));
};
