'use strict';

/**
 * Setup Promises!
 */

let BPromise = require('bluebird');

BPromise.promisifyAll(require('mongoose'));
