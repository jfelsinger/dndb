/**
 * Views
 * Setup the view engine
 */

let debug = require('debug')('dndb:server:middlewares:views');

let hbs = require('hbs'),
    moment = require('moment'),
    swag = require('swag'),
    slug = require('slug');

module.exports = function(app) {

    'use strict';

    let partialsPath = _root + '/../views/partials/';
    swag.Config.partialsPath = partialsPath;
    hbs.registerPartials(partialsPath);

    // Helpers
    hbs.registerHelper('json', JSON.stringify);

    hbs.registerHelper('moment', function(format, value) {
        // Just make it default to `de` in here
        moment.locale('de');
        return moment(value).format(format);
    });

    hbs.registerHelper('include', function(options) {
        let context = {},
            mergeContext = function(obj) {
                for (let k in obj) context[k] = obj[k];
            };

        mergeContext(this);
        mergeContext(options.hash);
        return options.fn(context);
    });

    hbs.registerHelper('dbg', function() {
        let args = Array.prototype.slice.call(arguments, 0, -1);
        debug('debug view:\r\n', args);
    });

    hbs.registerHelper('slug', function(text) {
        if (text === undefined) return text;
        return slug(text, '-');
    });

    swag.registerHelpers(hbs);

    debug('view enging setup');
    app.set('views', _root + '/../views');
    app.set('view engine', 'html');
    app.engine('html', hbs.__express);
};
