/**
 * Routing
 * Setup request routing
 */

let debug = require('debug')('dndb:request');

module.exports = function(app, db) {

    'use strict';

    // Allow all domains
    app.use(function(req, res, next) {
        let allowedMethods = [
            'GET', 'POST', 'OPTIONS',
            'PUT', 'PATCH', 'DELETE',
        ];

        let allowedHeaders = [
            'Origin',
            'Accept',
            'Content-Type',
            'x-access-token',
            'X-Access-Token',
            'X-Requested-With',
            'X-HTTP-Method-Override',
        ];

        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', allowedMethods.join(', '));
        res.header('Access-Control-Allow-Headers', allowedHeaders.join(', '));
        res.header('Access-Control-Allow-Credentials', true);

        debug('remoteAddress: ', req.connection.remoteAddress);
        debug('x-forwarded-for: ', req.headers['x-forwarded-for']);

        debug('req.method: ', req.method);
        if (req.method === 'OPTIONS')
            return res.send();

        next();
    });


    // ==== Process Routes ====

    // Routes handling static assets
    require(_root + '/config/routes/static')(app, db);

    // Routes handling api requests
    require(_root + '/config/routes/api')(app, db);

    // Routes handling application controllers
    require(_root + '/config/routes/controllers')(app, db);

};
