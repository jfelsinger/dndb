'use strict';

/**
 * Params
 * Automagically add parameters to the request
 */

let debug = require('debug')('dndb:config:params'),
    chalk = require('chalk');

module.exports = function(router, mongoose) {
    mongoose = mongoose || require('mongoose');

    [
        { name:'entry',   model: 'Entry' },
    ].forEach(function(row) {

        router.param(row.name + 'Id', function(req, res, next, id) {
            debug('getting: %s: ' + chalk.cyan('%s'), row.name, id);

            let Model = mongoose.model(row.model),
                query = Model.findById(id);


            query
                .execAsync()
                .then(function(obj) {
                    // Add result to the request

                    if (!obj) {
                        // capture.error('Failed to load %s: ' + chalk.cyan('%s'), row.name, id);
                        debug('Failed to load %s: ' + chalk.cyan('%s'), row.name, id);

                        return res.status(404).json({
                            code: '001404',
                            message: 'Object not found',
                            description: 'The requested `' + row.name + '` could not be found. \r\n' +
                                         'Object Id: ' + id + ' : NOT FOUND',
                            id: id
                        });
                    }

                    req[row.name] = obj;
                    next();
                })
                .catch(function(err) {
                    // capture.error(err);
                    debug(err);
                    return next(err);
                });


        });

    });

};
