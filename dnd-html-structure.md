

entry-category:

* associates
* backgrounds
* themes
* classes
* companions
* creatures
* deities
* diseases
* destinies
* feats
* items
* paragon paths
* poisons
* powers
* races
* rituals
* skills
* terrain
* traps



## Base
- name
- type
- sourcebook

## associates
- ...

## backgrounds
- campaign
- skills

## themes
- ...

## classes
- power source
- key abilities
- role name

## companions
- ...

## creatures
- level
- group role
- combat role

## deities
- Alignment

## diseases
- ...

## destinies
- prereqs

## feats
- tier name

## items
- cost
- level
- rarity
- item category

## paragon paths
- prereqs

## poisons
- cost
- level

## powers
- action type
- class name

## races
- size
- description attributes

## rituals
- level
- component cost
- price
- key skills

## skills
- type

## terrain
- type

## traps
- group role
- type
- level

