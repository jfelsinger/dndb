'use strict';

var gulp = require('gulp');
var async = require('async');

// scripts
var bower = require('gulp-bower'),
    browserify = require('browserify'),
    glob = require('glob'),
    path = require('path'),
    source = require('vinyl-source-stream'),
    jshint = require('gulp-jshint'),
    mocha = require('gulp-mocha'),
    istanbul = require('gulp-istanbul'),
    babel = require('gulp-babel');


// styles, etc.
var compass = require('gulp-compass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-csso'),
    imagemin = require('gulp-imagemin'),
    svgmin = require('gulp-svgmin'),
    rimraf = require('gulp-rimraf');


// Configuration Directories
var dir = {
    client: 'dndb-app/client',
    dist:   'dndb-app/dist',
    server: 'dndb-app/server',
};

/**
 * Handle errors in the pipe more gracefully
 */
function handleError(err) {
    /* jshint validthis:true */
    console.log(err.toString());
    this.emit('end');
}

gulp.task('rimraf', function() {
    return gulp.src(dir.dist, {read: false})
        .pipe(rimraf());
});

gulp.task('bower', function(cb) {
    return cb();
    return bower()
        .pipe(gulp.dest(dir.client + '/js/vendor'));
});



/* * * * * * * * *\
 * Style Tasks
\* * * * * * * * */

gulp.task('bower-styles', function(cb) {
    return cb();
    return bower({
        cwd: dir.client + '/sass'
    });
});

gulp.task('styles-sass', ['bower-styles'], function() {
    return gulp.src(dir.client + '/sass/*.{scss,sass}')
        .pipe(compass({
            style: 'expanded',
            css: dir.client + '/css',
            sass: dir.client + '/sass',
            require: [ ]
        }))
        .on('error', handleError)
        .pipe(autoprefixer())
        .pipe(gulp.dest(dir.client + '/css'));
});

gulp.task('styles', ['styles-sass'], function() {
    return gulp.src(dir.client + '/css/**')
        .pipe(minifycss())
        .pipe(gulp.dest(dir.dist + '/css'));
});



/* * * * * * * * *\
 * Script Tasks
\* * * * * * * * */

/**
 * Take an array of bundles to run through browserify
 */
function transpileModules(browserifyEntries, cb) {
    var cbs = [];

    browserifyEntries.forEach(function(entry) {
        cbs.push(function (cb) {
            var browserifyBundle = browserify({
                    entries: [entry.srcPath]
                });

            var finalStream = browserifyBundle
                .transform('babelify', {presets: ['es2015']})
                .on('error', handleError)
                .bundle()
                .on('error', handleError)
                .pipe(source(entry.outputFilename))
                .pipe(gulp.dest(entry.dest))
                .on('finish', cb);

                return finalStream;
        });
    });

    async.parallel(cbs, cb);
}

/**
 * Take a source path and find all files and creates the bundles to run
 * through browserify
 */
function handleScripts(scriptPath, ignores, cb) {
    var browserifyEntries = [];

    ignores = ignores.map(function(folder) {
        return scriptPath + '/' + folder + '/**';
    });

    var glub = scriptPath + '/**/*.js';
    var filepaths = glob.sync( glub, { ignore: ignores, });

    filepaths.forEach(function(filepath) {
        var filename = path.basename(filepath);
        var directoryOfFile = path.dirname(filepath);
        var relativeDirectory = path.relative(
            scriptPath,
            directoryOfFile);

        browserifyEntries.push({
            srcPath: './' + filepath,
            outputFilename: filename,
            dest: path.join(dir.client + '/js', relativeDirectory)
        });
    });

    transpileModules(browserifyEntries, cb);
}

gulp.task('scripts-browserify', function(cb) {
    // Set folders to ignore
    var ignore = [
        'components',
        'lib',
        'models',
        'modules',
        'templates',
        'vendor',
        'view-models',
    ];

    handleScripts(dir.client + '/scripts', ignore, cb);
});

gulp.task('include-scripts', function() {
    return gulp.src([
            dir.client + '/scripts/**/vendor/pusher.min.js',
            dir.client + '/scripts/**/vendor/fancybox/jquery.fancybox.pack.js',
        ])
        .pipe(gulp.dest(dir.client + '/js'));
});

gulp.task('scripts-client', ['scripts-browserify', 'include-scripts'], function() {
    return gulp.src(dir.client + '/js/**/*.js')
        // .pipe(uglify())
        .pipe(gulp.dest(dir.dist + '/js'));
});

gulp.task('scripts-server', function() {
    return gulp.src(dir.server + '/**/*.js')
        .pipe(babel({ presets:['es2015'] }))
        .pipe(gulp.dest(dir.server + '_bin'));
});



gulp.task('images', function() {
    return gulp.src(dir.client + '/images/**/*.{webp,png,jpg,jpeg}')
        .pipe(
            imagemin({
                optimizationLevel: 3,
                progressive: true,
                interlaced: true
            }))
        .pipe(gulp.dest(dir.dist + '/images/'));
});

gulp.task('gif', function() {
    return gulp.src(dir.client + '/img/**/*.gif')
        .pipe(gulp.dest(dir.dist + '/img/'));
});

gulp.task('svg', function() {
    return gulp.src(dir.client + '/images/**/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest(dir.dist + '/images/'));
});

gulp.task('fonts', function() {
    return gulp.src(dir.client + '/fonts/**')
        .pipe(gulp.dest(dir.dist + '/fonts/'));
});

gulp.task('watch-pot', ['mocha'], function() {
    gulp.watch('test/**/*.js', ['mocha']);
});

gulp.task('watch', ['build'], function() {

    // Watch scripts
    gulp.watch(dir.client + '/scripts/**/*.js', ['scripts-client']);
    gulp.watch(dir.server + '/**/*.js', ['scripts-server']);

    // Watch image files
    gulp.watch(dir.client + '/img/**/*.{png,jpg,jpeg}', ['images']);

    // Watch svg files
    gulp.watch(dir.client + '/img/**/*.svg', ['svg']);

    // Watch font files
    gulp.watch(dir.client + '/fonts/**', ['fonts']);

    // Watch styles
    gulp.watch([
            dir.client + '/sass/**/*.{sass,scss}',
        ], ['styles']);
});

gulp.task('mocha', function(cb) {
    gulp.src([
        'app/**/*.js',
        'config/**/*.js',
        'lib/**/*.js',
        // 'clients/scripts/**/*.js',
        // '!clients/scripts/vendor/**/*.js',
    ])
        .pipe(istanbul({
                includeUntested: true
            }))
        .pipe(istanbul.hookRequire())
        .on('error', handleError)
        .on('finish', function() {
            gulp.src('test/**/*.js')
                .pipe(mocha({ reporter: 'list' }))
                .on('error', handleError)
                .pipe(istanbul.writeReports())
                .on('end', cb);
        });
});

gulp.task('lint-tests', function() {
    return gulp.src([
            'test/**/*.js'
        ])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'));
});

gulp.task('lint', function() {
    return gulp.src([
            'gulpfile.js',
            'app/**/*.js',
            'config/**/*.js',
            'lib/**/*.js',
        ])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'));
});

gulp.task('lint-client', function() {
    return gulp.src([
            dir.client + '/scripts/**/*.js',
            '!' + dir.client + '/scripts/vendor/**',
        ])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'));
});

gulp.task('build', ['rimraf', 'bower'], function() {
    gulp.start('styles', 'scripts-server', 'scripts-client', 'images', 'gif', 'svg', 'fonts');
});

gulp.task('test', ['lint', 'mocha']);

/** Build it all up and serve it */
gulp.task('default', ['watch']);

// /** Build it all up and serve the production version */
// gulp.task('serve', ['app', 'client', 'watch']);
